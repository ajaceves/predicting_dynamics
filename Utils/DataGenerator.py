import numpy as np
import keras
import ntpath

class DataGenerator(keras.utils.Sequence):
    '''Generates data for Keras
    Do we want to load multiple movies at a time?, No. but if all movies are the same length, we can open one file after another sequentially.

    This version ignores the target channel, and just tries to predict the next frame. stacking of sequential frames occurs along the last axis
    '''
    def __init__(self, list_movies, n_frames_per_movie, n_context_frames, batch_size, dim=(28,28), shuffle = False):
        'Initialization'
        self.list_movies = list_movies
        self.n_frames_per_movie = n_frames_per_movie
        self.n_context_frames = n_context_frames
        self.batch_size = batch_size
        self.dim = dim
        self.shuffle = shuffle
        self.on_epoch_end()

    def __len__(self):
        'Denotes the number of batches per epoch'
        return int(np.floor((len(self.list_movies) * self.n_frames_per_movie) / self.batch_size))

    def __getitem__(self, index):
        'Generate one batch of data'
        # Generate indexes of the batch
        batch_indicies = self.indexes[index*self.batch_size:(index+1)*self.batch_size]
        # Generate data
        X, Y = self.__data_generation(batch_indicies)

        return X, Y

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.indexes = np.arange(len(self.list_movies)*self.n_frames_per_movie)
        if self.shuffle == True:
            np.random.shuffle(self.indexes)

    def __data_generation(self, batch_indicies):
        'Generates data containing batch_size samples' 
        # Initialization
        X = np.empty((self.batch_size, *self.dim, self.n_context_frames))
        Y = np.empty((self.batch_size, *self.dim))

        # Generate data
        file_number = batch_indicies[0] // self.n_frames_per_movie

        ''' A good idea to reduce reads, but memory is not shared between threads so it doesn't work
        if batch_indicies[0] % self.n_frames_per_movie == 0:
            file_handle = np.load(self.list_movies[file_number])
            file_handle = np.squeeze(file_handle)
        '''
        file_handle = np.load(self.list_movies[file_number])
        file_handle = np.squeeze(file_handle)

        for i, index in enumerate(batch_indicies):
            frame_number = index % self.n_frames_per_movie
            if frame_number == self.n_frames_per_movie - self.n_context_frames:
                X = np.resize(X, (self.batch_size - 2, *self.dim, self.n_context_frames))
                Y = np.resize(Y, (self.batch_size - 2, *self.dim))   
                break
            for j in range(self.n_context_frames):
                X[i,:,:,j] = file_handle[frame_number+j]
            #can't process last l frames... how to limit end of simulations?
            Y[i,] = file_handle[frame_number+self.n_context_frames]


        return X, Y

class StillStreamer(keras.utils.Sequence):
    '''Generates data for Keras for 2-channel images where second channel is target'''
    def __init__(self, list_files, n_frames_per_file, second_channel_as_target, batch_size, dim=(28,28), shuffle = False):
        'Initialization'
        self.list_files = list_files
        self.n_frames_per_file = n_frames_per_file
        self.second_channel_as_target = second_channel_as_target
        self.batch_size = batch_size
        self.dim = dim
        self.shuffle = shuffle
        self.on_epoch_end()

    def __len__(self):
        'Denotes the number of batches per epoch'
        return int(np.floor((len(self.list_files) * self.n_frames_per_file) / self.batch_size))

    def __getitem__(self, index):
        'Generate one batch of data'
        # Generate indexes of the batch
        batch_indicies = self.indexes[index*self.batch_size:(index+1)*self.batch_size]
        # Generate data
        X, Y = self.__data_generation(batch_indicies)

        return X, Y

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.indexes = np.arange(len(self.list_files)*self.n_frames_per_file)
        if self.shuffle == True:
            np.random.shuffle(self.indexes)

    def __data_generation(self, batch_indicies):
        'Generates data containing batch_size samples' 
        # Initialization

        # Generate data
        file_number = batch_indicies[0] // self.n_frames_per_file

        ''' A good idea to reduce reads, but memory is not shared between threads so it doesn't work
        if batch_indicies[0] % self.n_frames_per_movie == 0:
            file_handle = np.load(self.list_movies[file_number])
            file_handle = np.squeeze(file_handle)
        '''
        file_handle = np.load(self.list_files[file_number])
        file_handle = np.squeeze(file_handle)
        file_name = ntpath.basename(self.list_files[file_number])
        label = int(file_name.split('_')[6])
        if (label != 1) and (label != 0):
            raise ValueError('label is illegal!')
        
        if self.second_channel_as_target:
            X = np.empty((self.batch_size, *self.dim, 2))
        else:
            X = np.empty((self.batch_size, *self.dim, 1))

        Y = np.empty((self.batch_size))

        for i, index in enumerate(batch_indicies):
            frame_number = index % self.n_frames_per_file
            X[i,:,:,:] = file_handle[frame_number]

            Y[i] = label


        return X, Y

class MTLStreamer(keras.utils.Sequence):
    '''Generates data for Keras MTL'''
    def __init__(self, list_movies, list_files, n_frames_per_file, second_channel_as_target, n_frames_per_movie, n_context_frames, batch_size, dim=(28,28), shuffle = False):
        'Initialization'
        self.list_movies = list_movies
        self.list_files = list_files
        self.n_frames_per_file = n_frames_per_file
        self.second_channel_as_target = second_channel_as_target
        self.n_frames_per_movie = n_frames_per_movie
        self.n_context_frames = n_context_frames
        self.batch_size = batch_size
        self.dim = dim
        self.shuffle = shuffle
        self.on_epoch_end()

    def __len__(self):
        'Denotes the number of batches per epoch'
        return int(np.floor((len(self.list_movies) * self.n_frames_per_movie) / self.batch_size))

    def __getitem__(self, index):
        'Generate one batch of data'
        # Generate indexes of the batch
        batch_indicies = self.indexes[index*self.batch_size:(index+1)*self.batch_size]
        # Generate data from movies:
        X_list, Y_list = self.__data_generation(batch_indicies)
        return X_list, Y_list

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.indexes = np.arange(len(self.list_movies)*self.n_frames_per_movie)
        if self.shuffle == True:
            np.random.shuffle(self.indexes)

    def __data_generation(self, batch_indicies):
        'Generates data containing batch_size samples' 
        # Initialization
        X0 = np.empty((self.batch_size, *self.dim, self.n_context_frames))
        Y0 = np.empty((self.batch_size, *self.dim))

        # Generate data
        file_number = batch_indicies[0] // self.n_frames_per_movie
        file_handle = np.load(self.list_movies[file_number])
        file_handle = np.squeeze(file_handle)

        smaller_file = False
        for i, index in enumerate(batch_indicies):
            frame_number = index % self.n_frames_per_movie
            if frame_number == self.n_frames_per_movie - self.n_context_frames:
                X0 = np.resize(X0, (self.batch_size - 2, *self.dim, self.n_context_frames))
                Y0 = np.resize(Y0, (self.batch_size - 2, *self.dim)) 
                smaller_file = True  
                break

            for j in range(self.n_context_frames):
                X0[i,:,:,j] = file_handle[frame_number+j]
            #can't process last l frames... how to limit end of simulations?
            Y0[i,] = file_handle[frame_number+self.n_context_frames]

        ####START STILLS BLOCK####
        #need a correction factor for batch indicies... e.g. once you go past the end loop back
        file_number = batch_indicies[0] // self.n_frames_per_file
        file_number = file_number % len(self.list_files)
        file_handle = np.load(self.list_files[file_number])
        file_handle = np.squeeze(file_handle)
        file_name = ntpath.basename(self.list_files[file_number])
        label = int(file_name.split('_')[8])
        if (label != 1) and (label != 0):
            raise ValueError('label is illegal!')
        
        if self.second_channel_as_target:
            X1 = np.empty((self.batch_size, *self.dim, 2))
        else:
            X1 = np.empty((self.batch_size, *self.dim, 1))

        Y1 = np.empty((self.batch_size))

        for i, index in enumerate(batch_indicies):
            frame_number = index % self.n_frames_per_file
            X1[i,:,:,:] = file_handle[frame_number]
            Y1[i] = label

        if smaller_file:
            X1 = X1[:-self.n_context_frames]
            Y1 = Y1[:-self.n_context_frames]

        return [X0,X1], [Y0,Y1]

class StillStreamer1FPF(keras.utils.Sequence):
    '''Generates data for Keras for 2-channel images where second channel is target'''
    def __init__(self, still_frames, still_labels, second_channel_as_target, batch_size, dim=(28,28), shuffle = False):
        'Initialization'
        self.still_frames = still_frames
        self.still_labels = still_labels
        self.second_channel_as_target = second_channel_as_target
        self.batch_size = batch_size
        self.dim = dim
        self.shuffle = shuffle
        self.on_epoch_end()

    def __len__(self):
        'Denotes the number of batches per epoch'
        return int(np.floor((len(self.still_frames)) / self.batch_size))

    def __getitem__(self, index):
        'Generate one batch of data'
        # Generate indexes of the batch
        batch_indicies = self.indexes[index*self.batch_size:(index+1)*self.batch_size]
        # Generate data
        X, Y = self.__data_generation(batch_indicies)

        return X, Y

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.indexes = np.arange(len(self.still_frames))
        if self.shuffle == True:
            np.random.shuffle(self.indexes)

    def __data_generation(self, batch_indicies):
        'Generates data containing batch_size samples' 
        # Initialization

        # Generate data
        
        if self.second_channel_as_target:
            X = np.empty((self.batch_size, *self.dim, 2))
        else:
            X = np.empty((self.batch_size, *self.dim, 1))

        Y = np.empty((self.batch_size))

        for i, index in enumerate(batch_indicies):
            still_index = index % len(self.still_frames)
            X[i,:,:,:] = self.still_frames[still_index]

            label = int(self.still_labels[still_index])
            if (label != 1) and (label != 0):
                raise ValueError('label is illegal!')
            Y[i] = label

        return X, Y

class MTLStreamer1FPF(keras.utils.Sequence):
    '''Generates data for Keras MTL'''
    def __init__(self, list_movies, still_frames, still_labels, second_channel_as_target, n_frames_per_movie, n_context_frames, batch_size, n_frames_ahead = 0, dim=(28,28), shuffle = False):
        'Initialization'
        self.list_movies = list_movies
        self.still_frames = still_frames
        self.still_labels = still_labels
        self.second_channel_as_target = second_channel_as_target
        self.n_frames_per_movie = n_frames_per_movie
        self.n_context_frames = n_context_frames
        self.n_frames_ahead = n_frames_ahead
        self.batch_size = batch_size
        self.dim = dim
        self.shuffle = shuffle
        self.on_epoch_end()

    def __len__(self):
        'Denotes the number of batches per epoch'
        return int(np.floor((len(self.list_movies) * self.n_frames_per_movie) / self.batch_size))

    def __getitem__(self, index):
        'Generate one batch of data'
        # Generate indexes of the batch
        batch_indicies = self.indexes[index*self.batch_size:(index+1)*self.batch_size]
        # Generate data from movies:
        X_list, Y_list = self.__data_generation(batch_indicies)
        return X_list, Y_list

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.indexes = np.arange(len(self.list_movies)*self.n_frames_per_movie)
        if self.shuffle == True:
            np.random.shuffle(self.indexes)

    def __data_generation(self, batch_indicies):
        'Generates data containing batch_size samples' 
        # Initialization
        X0 = np.empty((self.batch_size, *self.dim, self.n_context_frames))
        Y0 = np.empty((self.batch_size, *self.dim))

        # Generate data
        self.file_number = batch_indicies[0] // self.n_frames_per_movie
        file_handle = np.load(self.list_movies[self.file_number])
        file_handle = np.squeeze(file_handle)

        smaller_file = False
        for i, index in enumerate(batch_indicies):
            frame_number = index % self.n_frames_per_movie
            if frame_number == (self.n_frames_per_movie - self.n_context_frames - self.n_frames_ahead):
                X0 = np.resize(X0, (self.batch_size - self.n_context_frames - self.n_frames_ahead, *self.dim, self.n_context_frames))
                Y0 = np.resize(Y0, (self.batch_size - self.n_context_frames - self.n_frames_ahead, *self.dim)) 
                smaller_file = True  
                break

            for j in range(self.n_context_frames):
                X0[i,:,:,j] = file_handle[frame_number+j]
            #can't process last l frames... how to limit end of simulations?
            Y0[i,] = file_handle[frame_number+self.n_context_frames+self.n_frames_ahead]

        ####START STILLS BLOCK####
        #need a correction factor for batch indicies... e.g. once you go past the end loop back     
                ####START STILLS BLOCK####
        #need a correction factor for batch indicies... e.g. once you go past the end loop back    
        if self.second_channel_as_target:
            X1 = np.empty((self.batch_size, *self.dim, 2))
        else:
            X1 = np.empty((self.batch_size, *self.dim, 1))

        Y1 = np.empty((self.batch_size))

        for i, index in enumerate(batch_indicies):
            still_index = index % len(self.still_frames)
            X1[i,:,:,:] = self.still_frames[still_index]

            label = int(self.still_labels[still_index])
            if (label != 1) and (label != 0):
                raise ValueError('label is illegal!')
            Y1[i] = label

        if smaller_file:
            X1 = X1[:-(self.n_context_frames+self.n_frames_ahead)]
            Y1 = Y1[:-(self.n_context_frames+self.n_frames_ahead)]

        return [X0,X1], [Y0,Y1]
