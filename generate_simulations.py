"""
Script for generating training data
"""

import math
import argparse

import pymunk
from pymunk import Vec2d
import numpy as np

def generate_training_data(n_links, anchor_position, target_position, output_path, n_sim_steps = 10000, max_size=28, link_size=2, output_targets = False):
    '''
    Aiden Aceves 2018

    Generate training movies as npy arrays. For now all balls are linked in center of top border
    
    Accepts:
    n_links : int
        the number of links to model
    target_position : tuple
        the position of the target to test and encode in simulation. must fall on one of the borders of the simulation.
    output_path : str
        location to save simulation data
    n_sim_steps : int
        the number of timesteps to run simulation for. default = 10,000
    max_size : int
        the size of the simulation space to setup, as the side of a square. default = 28        
    link_size : int
        the length in pixels of each link in the chain. default 2.
    output_targets : Bool
        Whether or not to create a second channel in each frame that denotes the "target" location. default = False

    Returns:
    Movies / trajectories of balls linked by linkers of length link_size as numpy arrays
    '''

    target_position = tuple(target_position)
    anchor_position = tuple(anchor_position)

    '''
    DISABLING POSITION CHECK
    if (target_position[0] != 1) and (target_position[1] != 1):
        raise ValueError('target position is illegal!')
    if (target_position[0] > max_size) or (target_position[1] > max_size):
        raise ValueError('target position is illegal!')
    '''
    output_path = output_path.rstrip('/') + str('/')

    space = pymunk.Space()
    space.gravity = (0.0, 0.0)
    space.damping=1

    ### outer bounds
    shape = pymunk.Segment(space.static_body,
                            (1, 1), (max_size, 1), 1.0)
    shape.friction = 0.0
    shape.elasticity = 1.
    space.add(shape)

    shape = pymunk.Segment(space.static_body,
                            (1, 1), (1, max_size), 1.0)
    shape.friction = 0.0
    shape.elasticity = 1.
    space.add(shape)

    shape = pymunk.Segment(space.static_body,
                            (max_size, 1), (max_size, max_size), 1.0)
    shape.friction = 0.0
    shape.elasticity = 1.
    space.add(shape)

    shape = pymunk.Segment(space.static_body, 
                            (max_size, max_size), (1, max_size), 1.0)
    shape.friction = 0.0
    shape.elasticity = 1.
    space.add(shape)

    #set anchor position, and use it to define delta_position and delta_position modifier
    anchor_x = anchor_position[0]
    anchor_y = anchor_position[1]
    
    if anchor_x is 1: #position is on left side
        if anchor_y is     
    elif anchor_x is 28: #position is on right side

    elif anchor_y is 1 #position is on bottom


    body = pymunk.Body(body_type=pymunk.Body.KINEMATIC)
    position = Vec2d(max_size/2,max_size)
    body.position = position

    ### links
    delta_position=Vec2d(-2*link_size/math.sqrt(2), -2*link_size/math.sqrt(2))
    position += delta_position

    for i in range(n_links):
        last_body = body
        mass = 1.0
        moment = pymunk.moment_for_circle(mass, inner_radius=0, outer_radius=0.5)
        body = pymunk.Body(mass, moment)
        body.position = position

        body.apply_impulse_at_local_point(Vec2d(tuple(np.random.normal(0,100,size=2))))
        shape = pymunk.Circle(body, radius=1)
        shape.friction = 0.0
        shape.elasticity = 1.
        space.add(body,shape)
        delta_position = Vec2d(delta_position * (-1,1))
        j = pymunk.PinJoint(body, last_body)
        space.add(j)
        position += delta_position
        position = Vec2d(position)

    shape = pymunk.Circle(space.static_body, 0.5, offset = position)
    shape.friction = 0.0
    shape.elasticity = 1.
    space.add(shape)
    #one pixel collision shape, larger on screen?
    max_reach = (n_links * link_size * 2) + 0.5 #add radius of ball
    distance_to_target = np.linalg.norm(np.array(target_position) - np.array([max_size/2,max_size]))

    reach_label = 0
    if max_reach >= distance_to_target:
        reach_label = 1

    if output_targets:
        simulation_array = np.zeros((n_sim_steps,max_size,max_size,2))
        simulation_array[:,int(target_position[0])-1,int(target_position[1])-1,0] = 1
        target_string = 'w_target_channel'
    else:
        simulation_array = np.zeros((n_sim_steps,max_size,max_size,1))
        target_string = ''

    for i in range(n_sim_steps):
      space.step(0.1 / 10000.0)
     
      for each in space._get_bodies():
        each = tuple(each.position - 1)
        if (each[0] > max_size) or (each[1] > max_size) or (each[0] < 1) or (each[1] <1):
            raise ValueError('simulation escaped its limits!')

        simulation_array[i,int(each[0]),int(each[1]),-1] = 1

    output_name = "_".join(['v1_dynamics_movies',str(target_position[0]),str(target_position[1]),str(n_links),str(reach_label),target_string,'.npy'])
    np.save(output_path + output_name, simulation_array)

def main():
    def str2bool(v):
        if v.lower() in ('yes', 'true', 't', 'y', '1'):
            return True
        elif v.lower() in ('no', 'false', 'f', 'n', '0'):
            return False
        else:
            raise argparse.ArgumentTypeError('Boolean value expected.')

    parser = argparse.ArgumentParser(description = 'Read in arguments relevant to generating simulations')
    parser.add_argument('--output_directory', type=str, help = 'Directory to save results to')
    parser.add_argument('--n_links', type=int, help = 'The maximum number of links to simulate in each movie', default = 8)
    parser.add_argument('--max_size', type=int, help = 'The size of simulation space to create', default = 28)
    parser.add_argument('--n_sim_steps', type=int, help = 'The number of frames contained in each movie', default = 10000)
    parser.add_argument('--second_channel_as_target', type=str2bool, nargs='?', const=False, help = 'Whether or not to write a second channel which shows the target position')

    args = parser.parse_args()

    output_directory = args.output_directory.rstrip('/')
    n_links = int(args.n_links)
    max_size = int(args.max_size)
    sim_steps = int(args.n_sim_steps)
    second_channel_as_target = args.second_channel_as_target

    anchor_position_list = []
    for i in range(2,max_size):
        i = int(i)
        anchor_position_list.append((i,max_size))

    if second_channel_as_target:
        target_position_list = []
        for i in range(1,max_size+1):
            i = int(i)
            target_position_list.append((1,i))
            target_position_list.append((max_size,i))
            target_position_list.append((i,1))
            target_position_list.append((i,max_size))

        for target_position in target_position_list:
            for length in range(1,n_links+1):
                for anchor_position in anchor_position_list:
                    generate_training_data(int(length), anchor_position, target_position, output_directory, n_sim_steps = sim_steps, output_targets = True)
    else:
       for length in range(1,n_links+1):
            for anchor_position in anchor_position_list:
                generate_training_data(int(length), anchor_position, (1,1), output_directory, n_sim_steps = sim_steps, output_targets = False)
        

if __name__ == "__main__":
    main()

#working
#do we need to draw lines?
#add second channel for target (if root will move it should be a third channel)?
#or inject after flattening?
#wrap as fn for different chain link lengths

#generate data on fly? store on scratch?
#network structure? initial task : given a bunch of static frames, predict 0/1
#add on task, flow


'''Random target

target_side = np.random.randint(4)
if target_side == 0:
    position = Vec2d(1.,np.random.randint(max_size))
elif target_side == 1:
    position = Vec2d(max_size,np.random.randint(max_size))
elif target_side == 2:
    position = Vec2d(np.random.randint(max_size),1)
elif target_side == 3:
    position = Vec2d(np.random.randint(max_size),max_size)
'''
