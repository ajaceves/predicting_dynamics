'''keras network to recreaate intuitive physicsnet'''
import glob
import time
import csv

#Third party import
import argparse
import numpy as np
import keras
from keras.layers import Conv2D, MaxPooling2D, Flatten, Dropout, Input, Dense, Activation, Reshape, Conv2DTranspose
from keras.models import Model, Sequential
from keras.losses import mean_squared_error
from keras.callbacks import EarlyStopping, TensorBoard, CSVLogger
from keras.optimizers import Adam
from keras import backend as K

#Custom modules
from Utils.DataGenerator import MTLStreamer
#need to have DataGenerator which returns list
#need to use validation set for predicting_target_hit_task

def main():
    def str2bool(v):
        if v.lower() in ('yes', 'true', 't', 'y', '1'):
            return True
        elif v.lower() in ('no', 'false', 'f', 'n', '0'):
            return False
        else:
            raise argparse.ArgumentTypeError('Boolean value expected.')

    parser = argparse.ArgumentParser(description = 'Read in arguments relevant to training')
    parser.add_argument('--movie_directory', type=str, help = 'Directory to search for movie files')
    parser.add_argument('--still_directory', type=str, help = 'Directory to search for movie files')
    parser.add_argument('--still_validation_directory', type=str, help = 'Directory to search for movie validation files')
    parser.add_argument('--n_frames_per_movie', type=int, help = 'The number of frames contained in each movie', default = 10000)
    parser.add_argument('--n_frames_per_file', type=int, help = 'The number of frames contained in each still file', default = 1000)
    parser.add_argument('--second_channel_as_target', type=str2bool, nargs='?', const=True, help = 'If true, second channel should contain target', default = True)
    parser.add_argument('--n_context_frames', type=int, help = 'The number of frames to make each prediction on', default = 2)
    parser.add_argument('--epochs', type=int, help = 'The Number of Epochs to Train For', default = 100)
    parser.add_argument('--batch_size', type=str, help = 'Batch size for neural net', default = 100)
    parser.add_argument('--learning_rate', type=float, help = 'Initial Learning Rate', default = 0.0001)
    parser.add_argument('--dropout', type=float, help = 'Dropout', default = 0.5)
    parser.add_argument('--n_filters', type=int, help = 'The Number of Filters to Use', default = 64)
    parser.add_argument('--loss_weights_a', help = 'how much to weight the first output', default = 1.)
    parser.add_argument('--loss_weights_b', help = 'how much to weight the second output', default = 1.)


    args = parser.parse_args()
    movie_directory = args.movie_directory
    list_movies = args.movie_directory.rstrip('/') + str('/*.npy')
    still_directory = args.still_directory.rstrip('/') + str('/*.npy')
    still_validation_directory = args.still_validation_directory.rstrip('/') + str('/*.npy')
    n_frames_per_movie = int(args.n_frames_per_movie)
    n_frames_per_file = int(args.n_frames_per_file)
    n_context_frames = int(args.n_context_frames)
    second_channel_as_target = args.second_channel_as_target
    epochs = int(args.epochs)
    batch_size = int(args.batch_size)
    learning_rate = float(args.learning_rate)
    dropout = float(args.dropout)
    n_filters = int(args.n_filters)
    l_weights_a = float(args.loss_weights_a)
    l_weights_b = float(args.loss_weights_b)

    movies = glob.glob(list_movies)
    list_files = glob.glob(still_directory)
    list_validation_files = glob.glob(still_validation_directory)
    
    #add validation

    data_streamer = MTLStreamer(movies, list_files, n_frames_per_file, second_channel_as_target, n_frames_per_movie, n_context_frames, batch_size=batch_size)
    validation_streamer = MTLStreamer(movies, list_validation_files, n_frames_per_file, second_channel_as_target, n_frames_per_movie, n_context_frames, batch_size=batch_size)

    #how to handle different number of training instances? how does this change what we call a batch?
    steps_per_epoch = (n_frames_per_movie * len(movies)) / batch_size
    
    #Setup Callbacks
    ES = EarlyStopping(monitor='loss', min_delta=0, patience=4, verbose=0, mode='min')

    #csv log setup
    stamp = int(time.time()*10000000)
    basename = '/home/aaceves/largeStorage/latent_spaces/logs/csvlogs/'
    filename = 'inutitive_physics_net_v1_' + str(stamp) + '.csv'
    csv_filename = basename + filename

    with open('/home/aaceves/largeStorage/latent_spaces/logs/csvlogs/parameters.csv', mode='a') as parameter_log:
        parameter_writer = csv.writer(parameter_log, delimiter=',', quotechar='"',   quoting=csv.QUOTE_MINIMAL)
        parameter_writer.writerow(['intuitive_physics_net_v1_w_validation', stamp, n_context_frames, epochs, batch_size, learning_rate, dropout, n_filters])

    CSV = CSVLogger(csv_filename, separator=',', append=False)

    tb_log_path = '/home/aaceves/largeStorage/latent_spaces/logs/tblogs/cross_net_w_validation_' + str(stamp) + '_context_' + str(n_context_frames) + '_LR_' + str(learning_rate) + '_n_filt_' + str(n_filters) + '_do_' + str(dropout) 
    TB = TensorBoard(log_dir=tb_log_path, histogram_freq=0, batch_size=batch_size, write_graph=True)  

    #Encoder:
    input_movie = Input(shape=(28,28,n_context_frames), name='movie_input')
    x = Conv2D(filters = n_filters, kernel_size = 4, strides = 2, activation='relu', padding='same', name='movie_encoder_conv1', data_format="channels_last")(input_movie)
    x = Dropout(dropout)(x)
    x = Conv2D(filters = n_filters, kernel_size = 4, strides = 2, activation='relu', padding='same', name='movie_encoder_conv2')(x)
    x = Dropout(dropout)(x)
    x = Conv2D(filters = n_filters, kernel_size = 4, strides = 2, activation='relu', padding='same', name='movie_encoder_conv3')(x)
    x = Dropout(dropout)(x)
    x = Flatten(name='movie_flatten')(x)


    input_still = Input(shape=(28,28,2), name='input_stills')
    s = Conv2D(filters = n_filters, kernel_size = 4, strides = 2, activation='relu', padding='same', name='still_encoder_conv1', data_format="channels_last")(input_still)
    s = Dropout(dropout)(s)
    s = Conv2D(filters = n_filters, kernel_size = 4, strides = 2, activation='relu', padding='same', name='still_encoder_conv2')(s)
    s = Dropout(dropout)(s)
    s = Conv2D(filters = n_filters, kernel_size = 4, strides = 2, activation='relu', padding='same', name='still_encoder_conv3')(s)
    s = Dropout(dropout)(s)
    s = Flatten(name='still_flatten')(s)

    x = keras.layers.concatenate([x, s])

    #Decoder:
    latent_space = Dense(98, activation='relu', name='latent_space')(x)
    x = Reshape((7,7,2))(latent_space)
    x = Conv2DTranspose(filters = n_filters, kernel_size = 4, strides = 2, activation='relu', padding='same', name='movie_decoder_deconv1')(x)
    x = Dropout(dropout)(x)
    x = Conv2DTranspose(filters = n_filters, kernel_size = 4, strides = 2, activation='relu', padding='same', name='movie_decoder_deconv2')(x)
    x = Dropout(dropout)(x)
    x = Conv2D(filters = 1, kernel_size = 1, strides = 1, activation='sigmoid', padding='same', name='decoder_output_conv')(x)
    output_movie = Reshape((28,28), name = 'movie_output')(x)

    d = Dense(196, activation='relu', name='still_fc1')(latent_space)
    d = Dropout(dropout)(d)
    d = Dense(196, activation='relu', name='still_fc2')(d)
    d = Dropout(dropout)(d)
    output_still = Dense(1, activation='sigmoid', name='still_output_neuron')(d)

    
    model = Model(inputs=[input_movie,input_still], outputs=[output_movie,output_still])
    adam = Adam(lr=learning_rate)
    alpha = K.variable(l_weights_a)
    beta = K.variable(l_weights_b)
    l_weights = [alpha,beta]
    model.compile(optimizer=adam, loss=['mean_squared_error','binary_crossentropy'], metrics=['accuracy'], loss_weights = l_weights)
    model.summary()
    model.fit_generator(data_streamer, validation_data = validation_streamer, epochs=epochs, steps_per_epoch = steps_per_epoch, shuffle = False, workers = 4, max_queue_size = 400, use_multiprocessing=True, callbacks=[CSV,TB,ES])

#CAN USE LOSS WEIGHTS!
#just make it work, dont care about preformance, then add validation
if __name__ == "__main__":
    main()
