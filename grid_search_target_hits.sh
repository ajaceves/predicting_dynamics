#!/bin/bash
for lr in 0.00001 0.000001
do
for filters in 64
do
for do in 0.3 0.5
do
for regularization in 0.001 0.0001
do
sbatch --gres gpu:1 --mem=128000 -t 12:00:00 -c 4 --wrap "python predicting_target_hits_1FPF.py --reg_rate $regularization --still_directory '/home/aaceves/largeStorage/latent_spaces/split_datasets/dataset_stills/' --still_validation_directory '/home/aaceves/largeStorage/latent_spaces/split_datasets/dataset_stills_validation/' --batch_size 200 --epochs 1000 --dropout $do --n_filters $filters --learning_rate $lr"
done
done
done
done
