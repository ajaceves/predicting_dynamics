#!/bin/bash
for lr in 0.00001 0.000001
do
for filters in 64 128
do
for context in 2 5 10
do
sbatch --gres gpu:1 --mem=128000 -t 12:00:00 -c 4 --wrap "python intuitive_physics_net_v1.py --movie_directory '/home/aaceves/largeStorage/latent_spaces/dataset_movies/' --batch_size 1000 --epochs 20 --dropout 0.5 --n_filters $filters --n_context_frames $context --learning_rate $lr"
done
done
done
