
#!/bin/bash
for lr in 0.00001 0.000001
do
for filters in 64
do
for context in 2
do
for do in 0.3
do
for loss_weights_a in 0.01 0.001
do
for loss_weights_b in 1.
do
for regularization in 0.001 0.0001
do
sbatch --gres gpu:1 --mem=128000 -t 12:00:00 -c 4 --wrap "python crossed_net_v1_double_DO_1FPF.py --reg_rate $regularization --movie_directory '/home/aaceves/largeStorage/latent_spaces/split_datasets/dataset_movies/' --still_directory '/home/aaceves/largeStorage/latent_spaces/split_datasets/dataset_stills/' --still_validation_directory '/home/aaceves/largeStorage/latent_spaces/split_datasets/dataset_stills_validation/' --batch_size 1000 --epochs 100 --dropout $do --n_filters $filters --n_context_frames $context --learning_rate $lr --loss_weights_a=$loss_weights_a --loss_weights_b=$loss_weights_b"
done
done
done
done
done
done
done
