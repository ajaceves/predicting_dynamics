'''keras network to recreaate intuitive physicsnet'''
import glob
import time
import csv

#Third party import
import argparse
import numpy as np
import keras
from keras.layers import Conv2D, MaxPooling2D, Flatten, Dropout, Input, Dense, Activation, Reshape, Conv2DTranspose
from keras.models import Model, Sequential
from keras.losses import mean_squared_error
from keras.callbacks import EarlyStopping, TensorBoard, CSVLogger
from keras.optimizers import Adam

#Custom modules
from Utils.DataGenerator import DataGenerator

def main():
    parser = argparse.ArgumentParser(description = 'Read in arguments relevant to training')
    parser.add_argument('--movie_directory', type=str, help = 'Directory to search for movie files')
    parser.add_argument('--n_frames_per_movie', type=int, help = 'The number of frames contained in each movie', default = 10000)
    parser.add_argument('--n_context_frames', type=int, help = 'The number of frames to make each prediction on', default = 2)
    parser.add_argument('--epochs', type=int, help = 'The Number of Epochs to Train For', default = 100)
    parser.add_argument('--batch_size', type=str, help = 'Batch size for neural net', default = 100)
    parser.add_argument('--learning_rate', type=float, help = 'Initial Learning Rate', default = 0.0001)
    parser.add_argument('--dropout', type=float, help = 'Dropout', default = 0.5)
    parser.add_argument('--n_filters', type=int, help = 'The Number of Filters to Use', default = 64)

    args = parser.parse_args()
    movie_directory = args.movie_directory
    list_movies = args.movie_directory.rstrip('/') + str('/*.npy')
    n_frames_per_movie = int(args.n_frames_per_movie)
    n_context_frames = int(args.n_context_frames)
    epochs = int(args.epochs)
    batch_size = int(args.batch_size)
    learning_rate = float(args.learning_rate)
    dropout = float(args.dropout)
    n_filters = int(args.n_filters)
    

    movies = glob.glob(list_movies)
    movie_streamer = DataGenerator(movies, n_frames_per_movie, n_context_frames, batch_size=batch_size)
    steps_per_epoch = (n_frames_per_movie * len(movies)) / batch_size
    
    #Setup Callbacks
    ES = EarlyStopping(monitor='loss', min_delta=0, patience=4, verbose=0, mode='min')

    #csv log setup
    stamp = int(time.time()*10000000)
    basename = '/home/aaceves/largeStorage/latent_spaces/logs/csvlogs/'
    filename = 'inutitive_physics_net_v1_' + str(stamp) + '.csv'
    csv_filename = basename + filename

    with open('/home/aaceves/largeStorage/latent_spaces/logs/csvlogs/parameters.csv', mode='a') as parameter_log:
        parameter_writer = csv.writer(parameter_log, delimiter=',', quotechar='"',   quoting=csv.QUOTE_MINIMAL)
        parameter_writer.writerow(['intuitive_physics_net_v1', stamp, movie_directory,n_frames_per_movie,n_context_frames,epochs,batch_size,learning_rate,dropout,n_filters])

    CSV = CSVLogger(csv_filename, separator=',', append=False)

    tb_log_path = '/home/aaceves/largeStorage/latent_spaces/logs/tblogs/' + str(stamp) + '_context_' + str(n_context_frames) + '_LR_' + str(learning_rate) + '_n_filt_' + str(n_filters) + '_do_' + str(dropout) 
    TB = TensorBoard(log_dir=tb_log_path, histogram_freq=0, batch_size=batch_size, write_graph=True)  

    #Encoder:
    input_all = Input(shape=(28,28,n_context_frames), name='input')
    x = Conv2D(filters = n_filters, kernel_size = 4, strides = 2, activation='relu', padding='same', name='encoder_conv1', data_format="channels_last")(input_all)
    x = Dropout(dropout)(x)
    x = Conv2D(filters = n_filters, kernel_size = 4, strides = 2, activation='relu', padding='same', name='encoder_conv2')(x)
    x = Dropout(dropout)(x)
    x = Conv2D(filters = n_filters, kernel_size = 4, strides = 2, activation='relu', padding='same', name='encoder_conv3')(x)
    x = Dropout(dropout)(x)
    x = Flatten(name='flatten')(x)

    #Decoder:
    x = Dense(98, activation='relu', name='fc1')(x)
    x = Reshape((7,7,2))(x)
    x = Conv2DTranspose(filters = n_filters, kernel_size = 4, strides = 2, activation='relu', padding='same', name='decoder_deconv1')(x)
    x = Dropout(dropout)(x)
    x = Conv2DTranspose(filters = n_filters, kernel_size = 4, strides = 2, activation='relu', padding='same', name='decoder_deconv2')(x)
    x = Dropout(dropout)(x)
    x = Conv2D(filters = 1, kernel_size = 1, strides = 1, activation='sigmoid', padding='same', name='decoder_output_conv')(x)
    #wtf he uses a flatten here? this is a weird network, probably better to skip that and just add channel at end
    #x = Flatten(name='flatten_2')(x)
    #x = Dense(900, activation='sigmoid', name='fc2')(x)
    main_output = Reshape((28,28))(x)
    
    model = Model(inputs=input_all, outputs=main_output)
    adam = Adam(lr=learning_rate)

    model.compile(optimizer=adam, loss='mean_squared_error')
    model.summary()
    model.fit_generator(movie_streamer, epochs=epochs, steps_per_epoch = steps_per_epoch, shuffle = False, workers = 4, max_queue_size = 400, use_multiprocessing=True, callbacks=[CSV,TB,ES])
    #model.fit(X, Y, epochs = 10)

if __name__ == "__main__":
    main()
