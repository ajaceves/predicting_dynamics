'''keras network to recreaate intuitive physicsnet'''
#TODO: Make the network predict n frames out! harder
#TODO: Examine effects of chaning physics simulations... e.g. slower sims?
import glob
import time
import csv

#Third party import
import argparse
import numpy as np
import keras
from keras.layers import Conv2D, MaxPooling2D, Flatten, Dropout, Input, Dense, Activation, Reshape, Conv2DTranspose
from keras.models import Model, Sequential
from keras.losses import mean_squared_error
from keras.callbacks import EarlyStopping, TensorBoard, CSVLogger
from keras.optimizers import Adam
from keras import backend as K
from keras import regularizers

#Custom modules
from Utils.DataGenerator import MTLStreamer1FPF
#need to have DataGenerator which returns list
#need to use validation set for predicting_target_hit_task

def main():
    def str2bool(v):
        if v.lower() in ('yes', 'true', 't', 'y', '1'):
            return True
        elif v.lower() in ('no', 'false', 'f', 'n', '0'):
            return False
        else:
            raise argparse.ArgumentTypeError('Boolean value expected.')

    parser = argparse.ArgumentParser(description = 'Read in arguments relevant to training')
    parser.add_argument('--n_samples', type=str, help = 'number of stills to subsample from training and validation')
    parser.add_argument('--movie_directory', type=str, help = 'Directory to search for movie files')
    parser.add_argument('--still_file', type=str, help = '.npy file of stills to train on')
    parser.add_argument('--still_file_labels', type=str, help = '.npy file labels for stills to train on')
    parser.add_argument('--still_validation_file', type=str, help = '.npy file of stills to test on')
    parser.add_argument('--still_validation_file_labels', type=str, help = '.npy file labels for stills to test on')
    parser.add_argument('--n_frames_per_movie', type=int, help = 'The number of frames contained in each movie', default = 10000)
    parser.add_argument('--second_channel_as_target', type=str2bool, nargs='?', const=True, help = 'If true, second channel should contain target', default = True)
    parser.add_argument('--n_context_frames', type=int, help = 'The number of frames to make each prediction on', default = 2)
    parser.add_argument('--n_frames_ahead', type=int, help = 'The number of frames ahead in the movie to predict', default = 0)
    parser.add_argument('--epochs', type=int, help = 'The Number of Epochs to Train For', default = 100)
    parser.add_argument('--batch_size', type=str, help = 'Batch size for neural net', default = 100)
    parser.add_argument('--learning_rate', type=float, help = 'Initial Learning Rate', default = 0.0001)
    parser.add_argument('--dropout', type=float, help = 'Dropout', default = 0.5)
    parser.add_argument('--n_filters', type=int, help = 'The Number of Filters to Use', default = 64)
    parser.add_argument('--loss_weights_a', help = 'how much to weight the first output', default = 1.)
    parser.add_argument('--loss_weights_b', help = 'how much to weight the second output', default = 1.)
    parser.add_argument('--reg_rate', help = 'l2 regularization', default = 0.001)

    args = parser.parse_args()
    n_samples = int(args.n_samples)
    movie_directory = args.movie_directory
    list_movies = args.movie_directory.rstrip('/') + str('/*.npy')
    still_file = args.still_file
    still_file_labels = args.still_file_labels
    still_validation_file = args.still_validation_file
    still_validation_file_labels = args.still_validation_file_labels
    n_frames_per_movie = int(args.n_frames_per_movie)
    n_context_frames = int(args.n_context_frames)
    frames_ahead = int(args.n_frames_ahead)
    second_channel_as_target = args.second_channel_as_target
    epochs = int(args.epochs)
    batch_size = int(args.batch_size)
    learning_rate = float(args.learning_rate)
    dropout = float(args.dropout)
    n_filters = int(args.n_filters)
    l_weights_a = float(args.loss_weights_a)
    l_weights_b = float(args.loss_weights_b)
    reg_rate = float(args.reg_rate)

    movies = glob.glob(list_movies)

    random_indicies = [3699, 2075, 840, 2540, 4009, 2266, 5189, 3467, 3202, 2207, 3150, 3828, 3414, 2818, 493, 3462, 3223, 5039, 3120, 4003, 446, 3751, 4678, 4834, 231, 2249, 869, 843, 3484, 5368, 4107, 3743, 355, 2234, 1835, 5119, 5339, 4922, 5169, 302, 2914, 4103, 3220, 674, 2926, 415, 3678, 4065, 2568, 958, 5479, 3143, 3989, 3285, 4371, 714, 2051, 1766, 3356, 4800, 2814, 4900, 2629, 4311, 693, 3321, 4063, 3648, 3078, 4396, 3401, 242, 4669, 5304, 2004, 4249, 4552, 951, 2088, 1390, 1750, 2921, 5464, 3387, 247, 3247, 479, 4198, 650, 4837, 307, 5321, 5288, 3852, 5505, 2060, 4580, 2711, 5078, 4782, 825, 4773, 2569, 412, 5134, 850, 168, 2791, 3923, 5021, 950, 1009, 4343, 212, 4658, 834, 5604, 3046, 1491, 1944, 795, 5177, 617, 1705, 1153, 205, 2437, 3808, 2392, 1576, 872, 1718, 574, 4487, 1844, 4716, 2168, 3347, 630, 3888, 4245, 2372, 837, 1064, 4104, 3376, 195, 1963, 2315, 1486, 3234, 211, 3709, 468, 1945, 5088, 976, 1100, 3115, 2503, 4468, 2543, 4118, 141, 996, 4318, 4287, 2460, 126, 5080, 3905, 4830, 591, 1103, 2084, 97, 499, 1639, 1271, 3463, 1915, 4762, 1631, 5049, 1467, 4036, 1087, 1166, 2124, 331, 3652, 3967, 5610, 112, 4532, 1955, 1513, 731, 4280, 937, 4823, 3422, 2343, 3849, 1228, 1527, 316, 1793, 2589, 1003, 236, 1736, 1434, 5331, 1597, 1432, 2970, 538, 519, 2679, 1948, 3133, 3215, 2748, 934, 3255, 3524, 4394, 514, 1355, 268, 4709, 4483, 2662, 5462, 71, 2270, 4796, 4151, 535, 2808, 4327, 2893, 3063, 2661, 4348, 4882, 4404, 1304, 5223, 1021, 2098, 4825, 2525, 5286, 3470, 4225, 2350, 2524, 2280, 5605, 3673, 310, 652, 2424, 4838, 4342, 2236, 4186, 1151, 5537, 4770, 2085, 3993, 2617, 1426, 5031, 1664, 1492, 1051, 807, 832, 1303, 5570, 4533, 2858, 5082, 373, 1257, 4182, 3937, 4273, 2985, 1815, 1088, 3238, 572, 5171, 3096, 3683, 1765, 870, 5365, 417, 102, 2773, 4663, 4997, 2493, 1078, 798, 4530, 2074, 645, 3553, 841, 4142, 5066, 2357, 1280, 403, 2196, 4921, 4925, 4276, 997, 4484, 1101, 4947, 2736, 2833, 5550, 4024, 702, 1624, 5148, 2762, 3539, 1989, 1516, 2530, 4215, 2763, 3431, 5307, 2386, 5451, 1047, 4445, 2144, 804, 2705, 5430, 4657, 1769, 4648, 2741, 4486, 4907, 175, 2932, 984, 4137, 3109, 4799, 610, 2631, 4301, 2800, 95, 3051, 1342, 1049, 2107, 2137, 459, 4268, 560, 1831, 1312, 4321, 1958, 3050, 1645, 386, 2761, 968, 333, 4606, 249, 604, 2528, 5511, 3367, 939, 1721, 2330, 2127, 357, 4833, 5184, 2371, 4850, 2188, 4450, 3630, 4597, 4457, 4676, 5087, 3483, 2161, 2693, 4793, 3985, 838, 1520, 4029, 4684, 1679, 3014, 218, 3425, 378, 4389, 1644, 2411, 4322, 3268, 4807, 5357, 2966, 2830, 1274, 5427, 3902, 809, 4813, 754, 682, 5051, 2251, 2903, 1146, 3313, 5150, 1643, 4629, 1014, 906, 5322, 5330, 933, 853, 1326, 2012, 3927, 1458, 3317, 2216, 761, 1478, 5221, 1544, 2837, 5377, 2916, 1992, 240, 2931, 1578, 3228, 1108, 943, 3707, 4547, 793, 1509, 3551, 3739, 758, 752, 5494, 4091, 339, 890, 622, 13, 19, 1974, 1044, 766, 2605, 1155, 3433, 1158, 4696, 29, 2398, 1007]

    train_stills = np.load(still_file)
    train_stills = np.squeeze(train_stills)
    train_stills_labels = np.load(still_file_labels)

    sample_train_stills = np.empty((n_samples,28,28,2))
    sample_train_labels = np.empty(n_samples)
    for i, rand_index in enumerate(random_indicies[:n_samples]):
        sample_train_stills[i:] = train_stills[rand_index]
        sample_train_labels[i] = train_stills_labels[rand_index]
	
    val_stills = np.load(still_validation_file)
    val_stills = np.squeeze(val_stills)
    val_stills_labels = np.load(still_validation_file_labels)  

    data_streamer = MTLStreamer1FPF(movies, sample_train_stills, sample_train_labels, second_channel_as_target, n_frames_per_movie, n_context_frames, n_frames_ahead = frames_ahead, batch_size=batch_size)
    validation_streamer = MTLStreamer1FPF(movies, val_stills, val_stills_labels, second_channel_as_target, n_frames_per_movie, n_context_frames, n_frames_ahead = frames_ahead, batch_size=batch_size)

    #how to handle different number of training instances? how does this change what we call a batch?
    steps_per_epoch = (n_frames_per_movie * len(movies)) / batch_size
    
    #Setup Callbacks
    ES = EarlyStopping(monitor='val_still_output_neuron_acc', min_delta=0.01, patience=4, verbose=1, mode='max')

    #csv log setup
    stamp = int(time.time()*10000000)
    basename = '/home/aaceves/largeStorage/latent_spaces/logs/csvlogs/'
    filename = 'crossed_nets_smaller_samples_' + str(stamp) + '.csv'
    csv_filename = basename + filename

    with open('/home/aaceves/largeStorage/latent_spaces/logs/csvlogs/parameters.csv', mode='a') as parameter_log:
        parameter_writer = csv.writer(parameter_log, delimiter=',', quotechar='"',   quoting=csv.QUOTE_MINIMAL)
        parameter_writer.writerow(['crossed_nets_smaller_samples_',stamp, n_samples, n_context_frames, frames_ahead, epochs, batch_size, learning_rate, dropout, n_filters, reg_rate, l_weights_a, l_weights_b])

    CSV = CSVLogger(csv_filename, separator=',', append=False)

    tb_log_path = '/home/aaceves/largeStorage/latent_spaces/logs/tblogs/crossed_nets_smaller_samples_full_val' + str(stamp) + '_n_samples_' + str(n_samples) + '_l2_' + str(reg_rate) + '_loss_weights_' + str(l_weights_a) + '-' + str(l_weights_b) + '_LR_' + str(learning_rate) + '_n_ahead_' + str(frames_ahead) + '_n_context_' + str(n_context_frames) + '_do_' + str(dropout) 
    TB = TensorBoard(log_dir=tb_log_path, histogram_freq=0, batch_size=batch_size, write_graph=True)  

    #Encoder:
    input_movie = Input(shape=(28,28,n_context_frames), name='movie_input')
    x = Conv2D(filters = n_filters, kernel_size = 4, strides = 2, activation='relu', kernel_regularizer = regularizers.l2(reg_rate), padding='same', name='movie_encoder_conv1', data_format="channels_last")(input_movie)
    x = Dropout(dropout)(x)
    x = Conv2D(filters = n_filters, kernel_size = 4, strides = 2, activation='relu', kernel_regularizer = regularizers.l2(reg_rate), padding='same', name='movie_encoder_conv2')(x)
    x = Dropout(dropout)(x)
    x = Conv2D(filters = n_filters, kernel_size = 4, strides = 2, activation='relu', kernel_regularizer = regularizers.l2(reg_rate), padding='same', name='movie_encoder_conv3')(x)
    x = Dropout(dropout)(x)
    x = Flatten(name='movie_flatten')(x)


    input_still = Input(shape=(28,28,2), name='input_stills')
    s = Conv2D(filters = n_filters, kernel_size = 4, strides = 2, activation='relu', kernel_regularizer = regularizers.l2(reg_rate), padding='same', name='still_encoder_conv1', data_format="channels_last")(input_still)
    s = Dropout(dropout)(s)
    s = Conv2D(filters = n_filters, kernel_size = 4, strides = 2, activation='relu', kernel_regularizer = regularizers.l2(reg_rate), padding='same', name='still_encoder_conv2')(s)
    s = Dropout(dropout)(s)
    s = Conv2D(filters = n_filters, kernel_size = 4, strides = 2, activation='relu', kernel_regularizer = regularizers.l2(reg_rate), padding='same', name='still_encoder_conv3')(s)
    s = Dropout(dropout)(s)
    s = Flatten(name='still_flatten')(s)

    x = keras.layers.concatenate([x, s])

    #Decoder:
    latent_space = Dense(98, activation='relu', name='latent_space')(x)
    x = Reshape((7,7,2))(latent_space)
    x = Conv2DTranspose(filters = n_filters, kernel_size = 4, strides = 2, activation='relu', padding='same', name='movie_decoder_deconv1')(x)
    x = Dropout(dropout)(x)
    x = Conv2DTranspose(filters = n_filters, kernel_size = 4, strides = 2, activation='relu', padding='same', name='movie_decoder_deconv2')(x)
    x = Dropout(dropout)(x)
    x = Conv2D(filters = 1, kernel_size = 1, strides = 1, activation='sigmoid', padding='same', name='decoder_output_conv')(x)
    output_movie = Reshape((28,28), name = 'movie_output')(x)

    d = Dense(196, activation='relu', kernel_regularizer = regularizers.l2(reg_rate), name='still_fc1')(latent_space)
    d = Dropout(dropout)(d)
    d = Dense(196, activation='relu', kernel_regularizer = regularizers.l2(reg_rate), name='still_fc2')(d)
    d = Dropout(dropout)(d)
    output_still = Dense(1, activation='sigmoid', name='still_output_neuron')(d)

    
    model = Model(inputs=[input_movie,input_still], outputs=[output_movie,output_still])
    adam = Adam(lr=learning_rate)
    alpha = K.variable(l_weights_a)
    beta = K.variable(l_weights_b)
    l_weights = [alpha,beta]
    model.compile(optimizer=adam, loss=['mean_squared_error','binary_crossentropy'], metrics=['accuracy'], loss_weights = l_weights)
    model.summary()
    model.fit_generator(data_streamer, validation_data = validation_streamer, epochs=epochs, steps_per_epoch = steps_per_epoch, shuffle = False, workers = 4, max_queue_size = 20, use_multiprocessing=True, callbacks=[CSV,TB,ES])

#CAN USE LOSS WEIGHTS!
#just make it work, dont care about preformance, then add validation
if __name__ == "__main__":
    main()
