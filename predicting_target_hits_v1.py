'''keras network to recreaate intuitive physicsnet'''
import glob
import time
import csv

#Third party import
import argparse
import numpy as np
import keras
from keras.layers import Conv2D, MaxPooling2D, Flatten, Dropout, Input, Dense, Activation, Reshape, Conv2DTranspose
from keras.models import Model, Sequential
from keras.losses import mean_squared_error
from keras.callbacks import EarlyStopping, TensorBoard, CSVLogger
from keras.optimizers import Adam
from keras import regularizers

#Custom modules
from Utils.DataGenerator import StillStreamer

def str2bool(v):
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

def main():
    parser = argparse.ArgumentParser(description = 'Read in arguments relevant to training')
    parser.add_argument('--still_directory', type=str, help = 'Directory to search for still images')
    parser.add_argument('--still_validation_directory', type=str, help = 'Directory to search for still validation images')
    parser.add_argument('--n_frames_per_file', type=int, help = 'The number of frames contained in each movie', default = 10000)
    parser.add_argument('--second_channel_as_target', type=str2bool, nargs='?', const=True, help = 'If true, second channel should contain target', default = True)
    parser.add_argument('--epochs', type=int, help = 'The Number of Epochs to Train For', default = 100)
    parser.add_argument('--batch_size', type=str, help = 'Batch size for neural net', default = 1000)
    parser.add_argument('--learning_rate', type=float, help = 'Initial Learning Rate', default = 0.00001)
    parser.add_argument('--dropout', type=float, help = 'Dropout', default = 0.5)
    parser.add_argument('--n_filters', type=int, help = 'The Number of Filters to Use', default = 64)
    parser.add_argument('--reg_rate', help = 'l2 regularization', default = 0.001)

    args = parser.parse_args()
    still_directory = args.still_directory.rstrip('/') + str('/*.npy')
    still_validation_directory = args.still_validation_directory.rstrip('/') + str('/*.npy')
    n_frames_per_file = int(args.n_frames_per_file)
    second_channel_as_target = args.second_channel_as_target
    epochs = int(args.epochs)
    batch_size = int(args.batch_size)
    learning_rate = float(args.learning_rate)
    dropout = float(args.dropout)
    n_filters = int(args.n_filters)
    reg_rate = float(args.reg_rate)
    

    list_files = glob.glob(still_directory)
    list_validation_files = glob.glob(still_validation_directory)
    #need new data generator which returns y as 0/1
    still_image_streamer = StillStreamer(list_files, n_frames_per_file, second_channel_as_target, batch_size=batch_size)
    validation_image_streamer = StillStreamer(list_validation_files, n_frames_per_file, second_channel_as_target, batch_size=batch_size)

    steps_per_epoch = (n_frames_per_file * len(list_files)) / batch_size
    
    #Setup Callbacks
    ES = EarlyStopping(monitor='accuracy', min_delta=0, patience=100, verbose=1, mode='max')

    #csv log setup
    stamp = int(time.time()*10000000)
    basename = '/home/aaceves/largeStorage/latent_spaces/logs/csvlogs/'
    filename = 'predicting_target_hits_v1' + str(stamp) + '.csv'
    csv_filename = basename + filename

    with open('/home/aaceves/largeStorage/latent_spaces/logs/csvlogs/parameters.csv', mode='a') as parameter_log:
        parameter_writer = csv.writer(parameter_log, delimiter=',', quotechar='"',   quoting=csv.QUOTE_MINIMAL)
        parameter_writer.writerow(['predicting_target_hits_v1_1FPF_l2', stamp, still_directory,n_frames_per_file,second_channel_as_target,epochs,batch_size,learning_rate,dropout,n_filters,reg_rate])

    CSV = CSVLogger(csv_filename, separator=',', append=False)

    tb_log_path = '/home/aaceves/largeStorage/latent_spaces/logs/tblogs/predicting_target_hits_v1_1FPF_w_l2_' + str(stamp) + '_l2_' + str(reg_rate) + '_LR_' + str(learning_rate) + '_n_filt_' + str(n_filters) + '_do_' + str(dropout) 

    TB = TensorBoard(log_dir=tb_log_path, histogram_freq=0, batch_size=batch_size, write_graph=True)  
    
    #Encoder:
    input_all = Input(shape=(28,28,2), name='input')
    x = Conv2D(filters = n_filters, kernel_size = 4, strides = 2, kernel_regularizer = regularizers.l2(reg_rate), activation='relu', padding='same', name='encoder_conv1', data_format="channels_last")(input_all)
    x = Dropout(dropout)(x)
    x = Conv2D(filters = n_filters, kernel_size = 4, strides = 2, kernel_regularizer = regularizers.l2(reg_rate), activation='relu', padding='same', name='encoder_conv2')(x)
    x = Dropout(dropout)(x)
    x = Conv2D(filters = n_filters, kernel_size = 4, strides = 2, kernel_regularizer = regularizers.l2(reg_rate), activation='relu', padding='same', name='encoder_conv3')(x)
    x = Dropout(dropout)(x)
    x = Flatten(name='flatten')(x)

    #Decoder:
    x = Dense(98, activation='relu', kernel_regularizer = regularizers.l2(reg_rate), name='fc1')(x)
    x = Dropout(dropout)(x)
    x = Dense(98, activation='relu', kernel_regularizer = regularizers.l2(reg_rate), name='fc2')(x)
    x = Dropout(dropout)(x)
    main_output = Dense(1, activation='sigmoid', name='output_neuron')(x)
    
    model = Model(inputs=input_all, outputs=main_output)
    adam = Adam(lr=learning_rate)

    model.compile(optimizer=adam, loss='binary_crossentropy', metrics = ['accuracy'])
    model.summary()
    model.fit_generator(still_image_streamer, validation_data = validation_image_streamer, epochs=epochs, steps_per_epoch = steps_per_epoch, shuffle = False, workers = 4, max_queue_size = 20, use_multiprocessing=True, callbacks=[CSV,TB])
    #model.fit(X, Y, epochs = 10)

if __name__ == "__main__":
    main()
